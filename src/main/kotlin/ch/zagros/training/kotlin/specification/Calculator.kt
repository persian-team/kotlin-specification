package ch.zagros.training.kotlin.specification

object Calculator {

    fun addition(n1: Int, n2: Int) = n1 + n2
    fun subtraction(n1: Int, n2: Int) = n1 - n2
    fun multipllication(n1: Int, n2: Int) = n1 * n2
    fun devision(n1: Int, n2: Int) = n1 / n2

}