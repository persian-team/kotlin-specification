import ch.zagros.training.kotlin.specification.Calculator
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import kotlin.test.assertSame

object CalculatorSpec : Spek({

    given("a calculator") {
        on("addition") {
            it("should return 7") {
                assertSame(7, Calculator.addition(2, 5))
            }
        }

        on("subtraction") {
            it("should return 5") {
                assertSame(5, Calculator.subtraction(10, 5))
            }
        }

        on("multiplication") {
            it("should return 25") {
                assertSame(25, Calculator.multipllication(5, 5))
            }
        }

        on("division") {
            it("should return 4") {
                assertSame(4, Calculator.devision(16, 4))
            }
        }
    }

})